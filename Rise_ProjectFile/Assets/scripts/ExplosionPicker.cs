﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionPicker : MonoBehaviour {
    public List<AudioClip> ExplosionList;
    public AudioSource AS;
	// Use this for initialization
	void Start () {
        AS = GetComponent<AudioSource>();
        AS.clip = ExplosionList[Random.Range(0, ExplosionList.Count)];
        AS.Play();
	}
}
