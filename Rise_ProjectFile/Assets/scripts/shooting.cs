﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shooting : MonoBehaviour
{
    public List<GameObject> AllNoneSingleRocks;
    public int NormalNumber;
    public int SingleENemyPoSITION;
    public List<Transform> enemySpawningLocation02;
    public int MaxSpawningLocations;
    public streatchy_arm ControllerScript;
    public GameObject enemyPrefab;
    public GameObject FinalPrefab;
    public RockScript Normalenemy;
    public RockScript finalEnemy;
    public RockScript SingleEnemy;
    public int SingleSpawnNumber;
    private float LevelUpTimer, NormalEnemyTimer, waveSpawnTimer, GrowTimer;
    public float LevelUpMax, NormalEnemyMax, waveSpawnMax, GrowTimerMax;
    public float currenttime;
    public bool Spawning;
    public List<BabyPhinx> ListOfbbyPhinx;
    public int currentBabyPhinx;
    public Transform FinalRockPosition;
    public bool FinalSpawnOnce = true;
    public List<Sprite> SpriteList;
    public List<GameObject> explosionList;
    public List<float> enemySpeedList;
    public int maxRepeatingRocks = 1;
    public int NumberofEnemies;
    public bool EndGame = false;
    public bool StopSpawning = false;
    public bool ShootAllBirds = false;
    public GameObject CloudDark;
    public GameObject CloudClear;
    public bool ChangeCoulds = false;
    public Transform AllRocksParent;
    public GameObject EnvSpawning;
    public Material DaytimeSkybox;
    public bool LastBbyGrow = true;
    public bool GrowFirst;
    public bool StopFirstGrowth;
    public Transform Parent;
    public int MaxSpeed = 0;
    public int maxSpawningSingle, maxSpawningRepeat;
	public GameObject ASMusic;

    public ParticleSystem duststormPS;
    public List<MeshRenderer> meshRenderEnviornment;
    public Material dayMat;
    // Use this for initialization
    void Start()
    {
        Spawn();
        Spawning = false;
        for (int i = 0; i < AllNoneSingleRocks.Count; i++)
        {
            RockScript rock = AllNoneSingleRocks[i].GetComponent<RockScript>();
            rock.SpawningScript = GetComponent<shooting>();
            rock.StartLocation = enemySpawningLocation02[Random.Range(0, MaxSpawningLocations)];
            int randomNumber = Random.Range(0, 3);
            rock.ColourRock.sprite = SpriteList[randomNumber];
            rock.ListofParticles = explosionList[randomNumber];
            rock.target = ControllerScript.gameObject;
        }
        ListOfbbyPhinx[0].Grow = true;
        currentBabyPhinx++;
    }
	
    // Update is called once per frame
    void Update()
    {
        currenttime += Time.deltaTime;
        LevelUpTimer += Time.deltaTime;
        GrowTimer += Time.deltaTime;
        if (ChangeCoulds == true)
        {
            CloudDark.SetActive(false);
        }
        if (currenttime >= 180)
        {
            EndGame = true;
        }
        if (EndGame == true)
        {
            /*
            if (LastBbyGrow == true)
            {
                ListOfbbyPhinx[3].Grow = true;
                LastBbyGrow = false;
            }
            */
            StopSpawning = true;
            if (NumberofEnemies <= 0)
            {
                ShootAllBirds = true;
                if (FinalSpawnOnce == true)
                {
                    FinalSpawn();
                    FinalSpawnOnce = false;
                }
            }
        }
        if (StopSpawning == false)
        {
            if (LevelUpTimer>=LevelUpMax)
            {
                if (ControllerScript.expandSpeed < 20)
                {
                    ControllerScript.expandSpeed += 5;
                    ControllerScript.retractSpeed += 5;
                    ControllerScript.IsUpgrading = true;
                }
                LevelUpTimer = 0;
            }
            if (StopFirstGrowth == false)
            {
                if (GrowFirst == true)
                {
                    GrowTimer = GrowTimerMax;
                    maxRepeatingRocks++;
                    SingleSpawnNumber++;
                    LevelUpTimer = LevelUpMax;
                    GrowFirst = false;
                    StopFirstGrowth = true;
                }
            }
            
            if (GrowTimer >= GrowTimerMax)
            {
                if (currentBabyPhinx < ListOfbbyPhinx.Count)
                {
                    ListOfbbyPhinx[currentBabyPhinx].Grow = true;
                    currentBabyPhinx++;
                }
                GrowTimerMax += 30;
                GrowTimer = 0;
            }
            if (Spawning == true)
            {
                Spawn();
                Spawning = false;
            }
            waveSpawnTimer += Time.deltaTime;
            if (waveSpawnTimer >= waveSpawnMax)
            {
                for (int i = 0; i < SingleSpawnNumber; i++)
                {
                    SpawnSingleEnemy();
                }
                waveSpawnTimer = 0;
            }

            NormalEnemyTimer += Time.deltaTime;
            if (NormalEnemyTimer >= NormalEnemyMax)
            {
                if (maxRepeatingRocks <= maxSpawningRepeat)
                {
                    Spawn();
                    NormalEnemyMax -= 2.5f;
                    maxRepeatingRocks++;
                }
                
                if (SingleSpawnNumber <= maxSpawningSingle)
                {
                    SingleSpawnNumber++;
                }
                if (MaxSpeed< enemySpeedList.Count)
                {
                    MaxSpeed++;
                }
                
                NormalEnemyTimer = 0;
            }
        }
        
    }
    public void Spawn()
    {
        if (StopSpawning == false)
        {
            GameObject enemy = AllNoneSingleRocks[NormalNumber];
            Normalenemy = enemy.GetComponent<RockScript>();
            Normalenemy.addEnemy = true;
            Normalenemy.speed = enemySpeedList[Random.Range(0, MaxSpeed)];
            NormalNumber++;
            Normalenemy.isSingleEnemy = false;
            enemy.SetActive(true);
            if (NormalNumber >= AllNoneSingleRocks.Count)
            {
                NormalNumber = 0;
            }
        }
    }
    public void SpawnSingleEnemy()
    {
        if (StopSpawning == false)
        {
            GameObject enemy = AllNoneSingleRocks[NormalNumber];
            enemy.SetActive(true);
            SingleEnemy = enemy.GetComponent<RockScript>();
            SingleEnemy.addEnemy = true;
            SingleEnemy.speed = enemySpeedList[Random.Range(0, MaxSpeed)];
            SingleEnemy.isSingleEnemy = true;
            NormalNumber++;
            if (NormalNumber >= AllNoneSingleRocks.Count)
            {
                NormalNumber = 0;
            }
        }
    }
    public void FinalSpawn()
    {
        GameObject Finalenemy = Instantiate(FinalPrefab, FinalRockPosition.position, FinalRockPosition.rotation, Parent);
        var Emission = duststormPS.emission;
        Emission.rateOverTime = 500f;
        finalEnemy = Finalenemy.GetComponent<RockScript>();
        finalEnemy.Parent = Parent;
        finalEnemy.StartLocation = finalEnemy.transform;
        finalEnemy.addEnemy = true;
        finalEnemy.SpawningScript = GetComponent<shooting>();
        finalEnemy.ColourRock.sprite = SpriteList[Random.Range(0, SpriteList.Count)];
        finalEnemy.target = ControllerScript.gameObject;
    }
    public void ChangeSkybox()
    {
        RenderSettings.skybox = DaytimeSkybox;
        EnvSpawning.SetActive(false);
		ASMusic.SetActive(false);
        var Emission = duststormPS.emission;
        Emission.rateOverTime = 0f;
        for (int i = 0; i < meshRenderEnviornment.Count; i++)
        {
            meshRenderEnviornment[i].material = dayMat;
        }
		ControllerScript.fadeOut();
    }
}
