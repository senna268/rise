﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NestSpawning : MonoBehaviour {
    public List<GameObject> fire_Particle;
    public List<GameObject> shell_objs;
    public void SpawnPhinex()
    {
        for (int i = 0; i < fire_Particle.Count; i++)
        {
            fire_Particle[i].SetActive(true);
        }
        Invoke("TurnOffShells", 1);
    }
    public void TurnOffShells()
    {
        for (int i = 0; i < shell_objs.Count; i++)
        {
            shell_objs[i].SetActive(false);
        }
    }
}
