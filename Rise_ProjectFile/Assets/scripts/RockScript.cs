﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockScript : MonoBehaviour {
    public GameObject target;
    public float speed;
    public shooting SpawningScript;
    public GameObject ListofParticles;
    public bool isSingleEnemy;
    public SpriteRenderer ColourRock;
    public bool isFinalRock;
    public bool nearSound = true;
    public Transform StartLocation;
    public bool addEnemy = false;
    public Transform Parent;
    public Transform ThisTransform;
    public AudioSource AS;
    // Use this for initialization
    void Start()
    {
        ThisTransform = transform;
        ThisTransform.LookAt(target.transform);
        AS = GetComponent<AudioSource>();
        AS.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * 10 * Time.deltaTime);
        if (addEnemy == true)
        {
            SpawningScript.NumberofEnemies++;
            ThisTransform.position = StartLocation.position;
            ThisTransform.LookAt(target.transform);
            addEnemy = false;
        }
        ThisTransform.position = Vector3.MoveTowards(ThisTransform.position, target.transform.position, speed * Time.deltaTime);
        if (isFinalRock == false)
        {
            
            if (SpawningScript.EndGame == true)
            {
                if (SpawningScript.NumberofEnemies <= 0)
                {
                    SpawningScript.NumberofEnemies--;
                    Instantiate(ListofParticles, ThisTransform.position, ThisTransform.rotation, Parent);
                    gameObject.SetActive(false);
                }
            }
            
        }
        if (Vector3.Distance(transform.position, target.transform.position)<15f)
        {
            AS.enabled = true;
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<ActualHand>() && !other.GetComponent<DestroyClose>())
        {
            return;
        }
        if (!other.GetComponent<DestroyClose>())
        {
            Instantiate(ListofParticles, ThisTransform.position, ThisTransform.rotation, Parent);
        }
        
        ThisTransform.position = StartLocation.position;
        SpawningScript.GrowFirst = true;
        if (isSingleEnemy == false)
        {
            SpawningScript.Spawn();
        }
        SpawningScript.NumberofEnemies--;
        if (isFinalRock == true)
        {
            SpawningScript.ChangeSkybox();
        }
        AS.enabled = false;
        gameObject.SetActive(false);
    }
}
