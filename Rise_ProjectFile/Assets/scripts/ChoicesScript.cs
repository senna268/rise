﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChoicesScript : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    public bool EasyMode;
    public bool HardMode;
    public streatchy_arm ControllerScript;
    public GameObject Particle;
    public GameObject easyMode;
    public GameObject hardMode;
    public ParticleSystem PSeasy;
    public ParticleSystem PShard;
    public Transform ThisTransform;
    public Animator Ani;
    public void Start()
    {
        ThisTransform = transform;
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        if (EasyMode == true)
        {
            ControllerScript.EasyDifficultyMode();
            Particle.SetActive(true);
        }
        if (HardMode == true)
        {
            ControllerScript.HardDifficultyMode();
            Particle.SetActive(true);
			
        }
        if (EasyMode == true)
        {
            easyMode.SetActive(false);
            PSeasy.gameObject.SetActive(false);
            PShard.gameObject.SetActive(false);
        }
        if (HardMode == true)
        {
            hardMode.SetActive(false);
            PSeasy.gameObject.SetActive(false);
            PShard.gameObject.SetActive(false);
        }
        
        
        Ani.Play("Exit");
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (EasyMode == true)
        {
            ThisTransform.localScale = new Vector3(0.2f, 0.5f, 1.1f);
            var emission = PSeasy.emission;
            emission.rateOverTime = 1000;
        }
        if (HardMode == true)
        {
            ThisTransform.localScale = new Vector3(0.2f, 0.5f, 1.1f);
            var emission = PShard.emission;
            emission.rateOverTime = 1000;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (EasyMode == true)
        {
            ThisTransform.localScale = new Vector3(0.175f, 0.46f, 1f);
            var emission = PSeasy.emission;
            emission.rateOverTime = 0;
        }
        if (HardMode == true)
        {
            ThisTransform.localScale = new Vector3(0.175f, 0.46f, 1f);
            var emission = PShard.emission;
            emission.rateOverTime = 0;
        }
    }
}
