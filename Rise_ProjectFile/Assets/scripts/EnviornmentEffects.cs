﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviornmentEffects : MonoBehaviour {
    public List<GameObject> MetroidEnvior;
    public int CurrentRock;
    public List<Transform> SpawnLocations;
    public List<Transform> MovementLocations;
    public bool isSpawning = false;
    public int numofSpawns;
    public List<float> ListofMaxTimers;
    public float currentMaxTimer;
    public float SpawnTimer;
    public Transform Player;
    public shooting ShootingScript;

    // Use this for initialization
    void Start ()
    {
        currentMaxTimer = 1;
        numofSpawns = 6;
        for (int i = 0; i < MetroidEnvior.Count; i++)
        {
            EnviornmentMeteriod Metro = MetroidEnvior[i].GetComponent<EnviornmentMeteriod>();
            Metro.startPosition = SpawnLocations[Random.Range(0, SpawnLocations.Count)];
            Metro.MovementLocation = MovementLocations[Random.Range(0, MovementLocations.Count)];
        }
    }
	// Update is called once per frame
	void Update ()
    {
		if (isSpawning == true)
        {
            for (int i = 0; i < numofSpawns; i++)
            {
                SpawnEffect();
            }
            isSpawning = false;
        }
        SpawnTimer += Time.deltaTime;
        if (SpawnTimer>=currentMaxTimer)
        {
            numofSpawns = Random.Range(1, 3);
            isSpawning = true;
            if (ShootingScript.EndGame == false)
            {
                currentMaxTimer = ListofMaxTimers[Random.Range(0, ListofMaxTimers.Count)];
            }
            else
            {
                currentMaxTimer = 1;
            }

            SpawnTimer = 0;
        }
	}
    public void SpawnEffect()
    {
        
        MetroidEnvior[CurrentRock].SetActive(true);
        MetroidEnvior[CurrentRock].GetComponent<EnviornmentMeteriod>().startrock = true;
        CurrentRock++;
        if (CurrentRock >= MetroidEnvior.Count)
        {
            CurrentRock = 0;
        }
        
    }
}
