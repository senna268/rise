﻿using System;
using System.Collections;
using System.Collections.Generic;
using Liminal.Core.Fader;
using Liminal.SDK.Core;
using Liminal.SDK.VR;
using Liminal.SDK.VR.Avatars;
using Liminal.SDK.VR.Input;
using UnityEngine;
using UnityEngine.SceneManagement;

public class streatchy_arm : MonoBehaviour {
    public List<ActualHand> ActiveHands;
    public ActualHand currentBird;
    public int activeHandNum;
    public bool canbesent = true;
	public float expandSpeed;
    public float retractSpeed;
	public bool comeback = false;
	public bool handpositions = true;
    public List<ActualHand> AllBirdReferences;
    public GameObject speedeffectObj;
    public shooting SpawningScript;
    public bool IsUpgrading = false;


    public Animator CurrentHandAni;
    public List<Animator> AniList;
    public List<ParticleSystem> PSList;
    public ParticleSystem CurrentPS;
    public List<GameObject> HandActive;
    public int ActiveHand;

    public bool AllChoicesMade;
    public GameObject ChoisesScreen;
    public AudioSource AS;
    // Use this for initialization
    void Start ()
    {

        handpositions = true;
        //UpdateHand();
        CurrentPS = PSList[ActiveHand];
        HandActive[ActiveHand].SetActive(true);
        CurrentHandAni = AniList[ActiveHand];
    }

    // Update is called once per frame
    void Update () 
	{
        if (AllChoicesMade == true)
        {
            if (!SpawningScript.gameObject.activeInHierarchy)
            {
                SpawningScript.gameObject.SetActive(true);
            }
            
            if (IsUpgrading == true)
            {
                AllBirdReferences[0].Upgrade();
                AllBirdReferences[1].Upgrade();
                AllBirdReferences[2].Upgrade();
                AllBirdReferences[3].Upgrade();
                IsUpgrading = false;
            }
            if (ActiveHands.Count != 0)
            {
                canbesent = true;
            }
            else
            {
                canbesent = false;
            }
            var primaryInput = VRDevice.Device.PrimaryInputDevice;
            if (SpawningScript.ShootAllBirds == false)
            {
                if (primaryInput.GetButtonDown(VRButton.One))
                {
                    if (canbesent == true)
                    {
                        AS.enabled = true;
                        var emitter = CurrentPS.emission;
                        emitter.rateOverTime = 500;
                        CurrentHandAni.Play("Open");
                        currentBird = ActiveHands[0];
                        ActiveHands.RemoveAt(0);
                        var BirdEmission = currentBird.PS.emission;
                        BirdEmission.rateOverTime = 150;
                        currentBird.follow = true;
                        currentBird.handpositions = false;
                        currentBird.Takeoff();
                        currentBird.expandSpeed = expandSpeed;
                        currentBird.retractSpeed = retractSpeed;
                    }
                }
                else if (primaryInput.GetButtonUp(VRButton.One))
                {
                    AS.enabled = false;
                    var emitter = CurrentPS.emission;
                    emitter.rateOverTime = 0;
                    if (currentBird != null)
                    {
                        CurrentHandAni.Play("Close");
                        currentBird.comeback = true;
                        var BirdEmission = currentBird.PS.emission;
                        BirdEmission.rateOverTime = 0;
                        currentBird = null;
                    }
                }
            }
            else
            {
                if (currentBird != null)
                {
                    currentBird.comeback = true;
                    currentBird = null;
                }
                if (ActiveHands.Count == 4)
                {
                    if (primaryInput.GetButtonDown(VRButton.One))
                    {
                        if (canbesent == true)
                        {
                            for (int i = 0; i < ActiveHands.Count; i++)
                            {
                                ActiveHands[i].follow = true;
                                ActiveHands[i].Final();
                                ActiveHands[i].handpositions = false;
                                ActiveHands[i].Takeoff();
                            }
                        }
                    }
                }
            }
        }
    }
    public void SpeedOn()
    {
        speedeffectObj.SetActive(true);
        Invoke("SpeedOff", 0.5f);
    }
    public void SpeedOff()
    {
        speedeffectObj.SetActive(false);
    }
	
    public void EasyDifficultyMode()
    {
        Debug.Log("easyMode");
        SpawningScript.maxSpawningRepeat = 3;
        SpawningScript.maxSpawningSingle = 4;
        SpawningScript.NormalEnemyMax = 25;
        SpawningScript.waveSpawnMax = 10;
        SpawningScript.MaxSpawningLocations = 30;
        CurrentHandAni.Play("Open");
        var emitter = CurrentPS.emission;
        emitter.rateOverTime = 1000;
        TurnOffRetnal();
        Invoke("StartExp", 2);
        
    }
    public void HardDifficultyMode()
    {
        Debug.Log("HardMode");
        SpawningScript.maxSpawningRepeat = 5;
        SpawningScript.maxSpawningSingle = 7;
        SpawningScript.NormalEnemyMax = 30;
        SpawningScript.waveSpawnMax = 8;
        SpawningScript.MaxSpawningLocations = SpawningScript.enemySpawningLocation02.Count;
        var emitter = CurrentPS.emission;
        emitter.rateOverTime = 1000;
        CurrentHandAni.Play("Open");
        TurnOffRetnal();
        Invoke("StartExp", 2);
    }
    public void UpdateHand()
    {
        
        if (VRAvatar.Active.PrimaryHand.InputDevice.Hand == VRInputDeviceHand.Right)
        {
            ActiveHand = 0;
        }
        else if (VRAvatar.Active.PrimaryHand.InputDevice.Hand == VRInputDeviceHand.Left)
        {
            ActiveHand = 1;
        }
        CurrentPS = PSList[ActiveHand];
        HandActive[ActiveHand].SetActive(true);
        CurrentHandAni = AniList[ActiveHand];
    }
    public void StartExp()
    {
        AllChoicesMade = true;
        ChoisesScreen.SetActive(false);
        var emitter = CurrentPS.emission;
        emitter.rateOverTime = 0;
        CurrentHandAni.Play("Close");
    }
    public void fadeOut()
    {
        for (int i = 0; i < HandActive.Count; i++)
        {
            HandActive[i].SetActive(false);
        }
        Invoke("EndExp", 5);
    }
    public void EndExp()
    {
        ExperienceApp.End();
        ScreenFader.Instance.FadeToBlack(3);
    }
    public void TurnOffRetnal()
    {
        
    }
}
