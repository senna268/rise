﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActualHand : MonoBehaviour {

    public streatchy_arm controller;
    public Transform FirstContact;
    public bool comeback = false;
    public float retractSpeed;
    public float expandSpeed;
    public bool handpositions;
    public bool follow = false;
    public Transform movementPoint;
    public Animator Ani;
    public Transform NextPosition;
    public bool isMovingtoController = true;
    public Transform EndPosition;
    public shooting SpawningScript;
    public GameObject FinalParticle;
    public List<GameObject> TrailList;
    public int currenttrailActive;
    public ParticleSystem PS;
    public Transform ThisTransform;
    public AudioSource AS;
    // Use this for initialization
    void Start () 
	{
        ThisTransform = transform;
        handpositions = true;
    }
	
	// Update is called once per frame
	void Update () 
	{
        if (handpositions == true)
        {
            ThisTransform.position = NextPosition.position;
            //Ani.Play("Idle");
            AS.enabled = false;
        }
        if (comeback == true)
        {
            isMovingtoController = true;
            follow = false;
            float steps = retractSpeed * Time.deltaTime;
            ThisTransform.position = Vector3.MoveTowards(ThisTransform.position, NextPosition.position, steps);
            ThisTransform.LookAt(NextPosition);
            if (Vector3.Distance(ThisTransform.position, NextPosition.position)<10f)
            {
                Ani.Play("Landing");
            }
            if (NextPosition.position == transform.position)
            {
                handpositions = true;
                controller.ActiveHands.Add(this.GetComponent<ActualHand>());
                comeback = false;

            }
        }
        if (follow == true)
        {
            AS.enabled = true;
            if (isMovingtoController == true)
            {
                handpositions = false;
                if (SpawningScript.EndGame == false)
                {
                    ThisTransform.position = Vector3.MoveTowards(ThisTransform.position, FirstContact.position, 20 * Time.deltaTime);
                    ThisTransform.LookAt(FirstContact);
                    if (ThisTransform.position == FirstContact.position)
                    {
                        isMovingtoController = false;
                    }
                }
                else
                {
                    ThisTransform.position = Vector3.MoveTowards(ThisTransform.position, EndPosition.position, expandSpeed * Time.deltaTime);
                    ThisTransform.LookAt(EndPosition.transform);
                    if (ThisTransform.position == EndPosition.position)
                    {
                        isMovingtoController = false;
                    }
                }
                
            }
            if (isMovingtoController == false)
            {
                handpositions = false;
                float step = expandSpeed * Time.deltaTime;
                ThisTransform.position = Vector3.MoveTowards(ThisTransform.position, movementPoint.transform.position, step);
                ThisTransform.rotation = controller.transform.rotation;
                
            }
            
        }
        
    }
    public void Takeoff()
    {
        Ani.Play("TakeOff");
    }



    public void Upgrade()
    {
        if (currenttrailActive == 0)
        {
            TrailList[0].SetActive(true);
        }
        if (currenttrailActive == 1)
        {
            TrailList[1].SetActive(true);
            TrailList[2].SetActive(true);
        }
        currenttrailActive++;
    }
    public void Final()
    {
        Instantiate(FinalParticle, ThisTransform.position, ThisTransform.rotation, transform);
    }
}
