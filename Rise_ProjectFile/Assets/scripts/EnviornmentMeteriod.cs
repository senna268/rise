﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnviornmentMeteriod : MonoBehaviour
{
    public Transform MovementLocation;
    public float speed;
    public GameObject ListofParticles;
    public Transform Rock;
    public bool Once = true;
    public Transform startPosition;
    public bool startrock = false;
    public Transform Parent;
    public Transform ThisTransform;
    // Use this for initialization
    void Start()
    {
        ThisTransform = transform;
        ThisTransform.LookAt(MovementLocation);

    }

	// Update is called once per frame
	public void LateUpdate()
	{
		if (startrock == true)
		{
			ThisTransform.position = startPosition.position;
			ThisTransform.LookAt(MovementLocation);
			Once = true;
			startrock = false;
		}

		ThisTransform.position = Vector3.MoveTowards(ThisTransform.position, MovementLocation.position, speed * Time.deltaTime);
		if (Vector3.Distance(ThisTransform.position, MovementLocation.position) < 1f)
		{
			if (Once == true)
			{
				Instantiate(ListofParticles, ThisTransform.position, ThisTransform.rotation, Parent);
				ThisTransform.position = startPosition.position;
				gameObject.SetActive(false);
				Once = false;
			}
		}
	}
}
