﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyClose : MonoBehaviour {
    public GameObject redscreen;
    public shooting spawningScript;
    public Transform Player;
    public float timer;
    public void Update()
    {
        if (redscreen.activeInHierarchy)
        {

            timer += Time.deltaTime;
            if (timer > 0.2f)
            {
                redscreen.SetActive(false);
                timer = 0;
            }
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<RockScript>())
        {
            return;
        }
        else
        {
            if (redscreen.activeInHierarchy)
            {
                redscreen.SetActive(false);
                redscreen.SetActive(true);
            }
            else
            {
                redscreen.SetActive(true);
            }
        }
    }
}
