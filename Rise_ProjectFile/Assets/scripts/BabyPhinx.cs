﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BabyPhinx : MonoBehaviour {
    public bool Grow = false;
    public streatchy_arm ArmScript;
    public ActualHand thisHandScript;
    public Animator Ani;
    public Transform WaitingPosition;
    public bool spawnOnce = true;
    public float Timer;
    public NestSpawning nestScript;
    public Transform ThisTransform;
    public AudioSource AS;
    // Use this for initialization
    void Start () {

        ThisTransform = transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if (Grow == true)
        {
            
            if (spawnOnce == true)
            {
                nestScript.SpawnPhinex();
                spawnOnce = false;
            }
            Timer += Time.deltaTime;
            if (Timer>=3)
            {
                
                if (ThisTransform.localScale.x < 0.4f)
                {
                    ThisTransform.localScale += new Vector3(0.1f, 0.1f, 0.1f) * Time.deltaTime;
                    ThisTransform.position = Vector3.MoveTowards(ThisTransform.position, WaitingPosition.position, 1 * Time.deltaTime);
                    Ani.Play("Landing");
                    AS.enabled = true;
                }
                if (ThisTransform.localScale.x >= 0.4f)
                {
                    thisHandScript.enabled = true;
                    ArmScript.ActiveHands.Add(thisHandScript);
                    Ani.Play("Idle");
                    Grow = false;
                    AS.enabled = false;
                }
            }
            
        }
	}
}
